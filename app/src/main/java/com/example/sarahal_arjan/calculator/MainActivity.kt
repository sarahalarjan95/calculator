package com.example.sarahal_arjan.calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mZero.setOnClickListener {
            handleClick("0")
        }

        mOne.setOnClickListener {
            handleClick("1")
        }
        mTwo.setOnClickListener {
            handleClick("2")
        }
        mThree.setOnClickListener {
            handleClick("3")
        }
        mFour.setOnClickListener {
            handleClick("4")
        }
        mFive.setOnClickListener {
            handleClick("5")
        }

        mSix.setOnClickListener {
            handleClick("6")
        }

        mSeven.setOnClickListener {
            handleClick("7")
        }
        mEight.setOnClickListener {
            handleClick("8")
        }
        mNine.setOnClickListener {
            handleClick("9")
        }
        mDot.setOnClickListener {
            handleClick(".")
        }
        mClose.setOnClickListener {
            handleClick("(")
        }
        mOpen.setOnClickListener {
            handleClick(")")
        }
        mDivide.setOnClickListener {
            handleClick("/")
        }
        mMinus.setOnClickListener {
            handleClick("-")
        }
        mPlus.setOnClickListener {
            handleClick("+")
        }
        mMul.setOnClickListener {
            handleClick("*")
        }
        mCE.setOnClickListener {
            mTextViewOperation.text=""
            mTextViewValue.text=""
        }

        mBackspace.setOnClickListener {
           val string=mTextViewOperation.text.toString()
            mTextViewOperation.text=string.substring(0,string.length-1)
        }


        mEqual.setOnClickListener {


        try {

            val expression = ExpressionBuilder(mTextViewOperation.text.toString()).build()
            val result = expression.evaluate()
            val longResult = result.toLong()
            if(result == longResult.toDouble())
                mTextViewValue.text = longResult.toString()
            else
                mTextViewValue.text = result.toString()

        }catch (e:Exception){
            Log.d("Exception"," message : " + e.message )
        }

        }







            }


    fun handleClick(value: String) {

        mTextViewOperation.append(value)
    }
        }


